package Week2;

import edu.princeton.cs.algs4.*;

import java.util.Arrays;

public class TwoSum {
  public static void main(String[] args) {
    In in = new In("lib/algs4/32Kints.txt");
    int[] a = in.readAllInts();
    int n = a.length;
    Arrays.sort(a, 0, n);
    for (int i = 0; a[i] < 0; i++) {
      for (int j = i + 1; j < n; j++) {
        if (a[i] < 0 && a[j] < 0) {
          continue;
        }
        if (a[i] + a[j] == 0) {
          System.out.println(a[i] + " " + a[j]);
          i++;
          j = i + 1;
        }
      }
    }
  }
}
