package Week2;

import edu.princeton.cs.algs4.In;

import java.util.Arrays;

public class ThreeSum {
  public static void main(String[] args) {
    In in = new In(args[0]);
    int[] a = in.readAllInts();
    int n = a.length;
    Arrays.sort(a, 0, n);
    for (int i = 0; a[i] < 0; i++) {
      for (int j = i + 1; j < n - 1; j++) {
        for (int k = j + 1; k < n; k++) {
          if (a[i] + a[j] + a[k] == 0) {
            System.out.println(a[i] + " " + a[j] + " " + a[k]);
            j++;
            k = j + 1;
          }
        }
      }
    }
  }
}
