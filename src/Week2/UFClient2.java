package Week2;

import edu.princeton.cs.algs4.*;

public class UFClient2 {
  public static void main(String[] args) {
    int N = StdIn.readInt();
    UF uf = new UF(N);
    int count = 0;
    while (!StdIn.isEmpty()) {
      int p = StdIn.readInt();
      int q = StdIn.readInt();
      if (!uf.connected(p, q)) {
        uf.union(p, q);
      }
      count++;
      if (count >= N - 1) {
        int check = 0;
        for (int i = 0; i < N - 1; i++) {
          if (uf.connected(i, i + 1)) {
            check++;
          }
          if (check == N - 1) {
            System.out.println(count);
            break;
          }
        }
      }
    }
  }
}
