package Week4;

public class LinkedList {
  Node head;

  static class Node {
    int data;
    Node next;

    Node(int x) {
      this.data = x;
      this.next = null;
    }
  }

  /** Print Out The Linked List. */
  public static void print(LinkedList list) {
    Node current = list.head;
    System.out.print("[");
    while (current != null) {
      System.out.print(current.data + ", ");
      current = current.next;
    }
    System.out.println("]");
  }

  /** Insert At The Head */
  public static LinkedList insertFirst(LinkedList list, int data) {
    Node new_head = new Node(data);
    if (list.head != null) {
      new_head.next = list.head;
    }
    list.head = new_head;
    return list;
  }

  /** Insert At The End. */
  public static LinkedList insertTail(LinkedList list, int data) {
    Node new_head = new Node(data);
    if (list.head == null) {
      list.head = new_head;
      return list;
    } else {
      Node current = list.head;
      while (current.next != null) {
        current = current.next;
      }
      current.next = new_head;
    }
    return list;
  }

  /** Insert After N Position. */
  public static LinkedList insertAfter(LinkedList list, int data, int position) {
    Node new_node = new Node(data);
    if (position < 0) {
      System.out.println("not a valid position");
      return list;
    } else if (position == 0) {
      insertFirst(list, data);
      return list;
    } else {
      Node current = list.head;
      for (int i = 1; i < position && current.next != null; i++) {
        current = current.next;
      }
      if (current.next != null) {
        new_node.next = current.next;
      }
      current.next = new_node;
      return list;
    }
  }

  /** Delete A Node. */
  public static LinkedList deleteAfter(LinkedList list, int position) {
    if (position < 0) {
      System.out.println("Not A Valid Position");
      return list;
    } else if (position == 0) {
      list.head = list.head.next;
      return list;
    } else {
      Node temp = list.head;
      for (int i = 1; i < position && temp.next.next != null; i++) {
        temp = temp.next;
      }
      temp.next = temp.next.next;
      return list;
    }
  }

  /** Print The Linked List In Reverse (Need two parts, One Recursive, One Don't). */
  public static void printReverse(LinkedList list) {
    System.out.print("[");
    recursivePrint(list.head);
    System.out.println("]");
  }

  /** The Recursive Part Of Reverse Printing. */
  private static void recursivePrint(Node head) {
    if (head != null) {
      recursivePrint(head.next);
      System.out.print(head.data + ", ");
    }
  }

  /** Create A New Reversed Linked List. */
  public static LinkedList reverse(LinkedList list) {
    LinkedList new_list = new LinkedList();
    Node temp = list.head;
    while (temp != null) {
      insertFirst(new_list, temp.data);
      temp = temp.next;
    }
    return new_list;
  }

  /** Compare Two Linked List. */
  public static boolean compareList(LinkedList list1, LinkedList list2) {
    while ((list1.head != null && list2.head != null) && list1.head.data == list2.head.data) {
      list1.head = list1.head.next;
      list2.head = list2.head.next;
    }
    return (list1.head == null && list2.head == null);
  }

  /** Merge Two Sorted List Together. */
  public static LinkedList mergeList(LinkedList list1, LinkedList list2) {
    if (list1.head == null && list2.head == null) {
      return new LinkedList();
    } else if (list1.head == null) {
      return list2;
    } else if (list2.head == null) {
      return list1;
    }
    if (list1.head.data > list2.head.data) {
      return mergeList(list2, list1);
    }
    LinkedList new_list1 = new LinkedList();
    new_list1.head = list1.head;
    while (list2.head != null) {
      while (list1.head.next != null && list2.head.data > list1.head.next.data) {
        list1.head = list1.head.next;
      }
      Node next_list2 = list2.head.next;
      list2.head.next = list1.head.next;
      list1.head.next = list2.head;
      list2.head = next_list2;
    }
    return new_list1;
  }

  /** Get The Node N Position From the Tail. */
  public static int getNodeFromTail(LinkedList list, int position) {
    Node current = list.head;
    Node tail = list.head;
    int count = 0;
    while (current.next != null) {
      current = current.next;
      if (count >= position) {
        tail = tail.next;
      }
      count++;
    }
    return tail.data;
  }
}
