package Week5;

import java.util.NoSuchElementException;

public class MyOwnQueue {
  public static class LinkedListQueue {
    Node first;
    Node last;
    int num;

    public boolean isEmpty() {
      return this.first == null;
    }

    public int getSize() {
      return this.num;
    }

    public int peek() {
      if (this.isEmpty()) {
        throw new NoSuchElementException("The Stack Is Empty?? :o");
      }
      return this.first.data;
    }

    public void enqueue(int data) {
      Node old_tail = last;
      last = new Node(data);
      if (this.isEmpty()) {
        first = last;
      } else {
        old_tail.next = last;
      }
      this.num++;
    }

    public int dequeue() {
      if (this.isEmpty()) {
        throw new NoSuchElementException("Are You Trying to dequeue an empty queue :)???");
      }
      int return_data = first.data;
      first = first.next;
      num--;
      if (this.isEmpty()) {
        last = null;
      }
      return return_data;
    }
  }

  public class ArrayQueue {
    private static final int default_size = 8;

    private Integer[] array;
    private int numElement;
    private int first;
    private int last;

    public ArrayQueue() {
      this.array = new Integer[default_size];
      this.numElement = 0;
      this.first = 0;
      this.last = 0;
    }

    public boolean isEmpty() {
      return numElement == 0;
    }

    public int size() {
      return numElement;
    }

    private void resize(int new_size) {
      assert new_size >= numElement;
      Integer[] new_array = new Integer[new_size];
      for (int i = 0; i < numElement; i++) {
        new_array[i] = array[(first + i) % array.length];
      }
      array = new_array;
      first = 0;
      last = numElement;
    }

    public void enQueue(int data) {
      if (numElement == array.length) {
        resize(2 * array.length);
      }
      array[last++] = data;
      if (last == array.length) {
        last = 0;
      }
      numElement++;
    }

    public int deQueue() {
      if (isEmpty())
        throw new NoSuchElementException("The Queue Is Empty :( So Put Something In It First :v");
      int return_data = array[first];
      array[first] = null;
      numElement--;
      first++;
      if (first == array.length) first = 0;
      if (numElement > 0 && numElement == array.length / 4) resize(array.length / 2);
      return return_data;
    }

    public int peek() {
      if (isEmpty())
        throw new NoSuchElementException("The Queue Is Empty :( So Put Something In It First :v");
      return array[first];
    }
  }

  public static class Node {
    int data;
    Node next;

    public Node(int data) {
      this.data = data;
      this.next = null;
    }
  }
}
