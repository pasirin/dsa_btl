package Week5;

public class SelectionSort {
  public static void main(String[] args) {
    int[] a = {
      9, 8, 5, 2, 1, 3, 13, 131, 4154, 4534, 36547, 86867, 4423, 342342, 454, 657, 89, 98, 90, 112
    };

    long start = System.currentTimeMillis();
    for (int i = 0; i < a.length; i++) {
      int min = i;
      for (int j = i + 1; j < a.length; j++) {
        if (a[j] < a[i]) {
          min = j;
        }
      }
      int temp = a[i];
      a[i] = a[min];
      a[min] = temp;
    }
    long end = System.currentTimeMillis();

    System.out.println(start);
    System.out.println(end);
    for (int i = 0; i < a.length; i++) {
      System.out.print(a[i] + " ");
    }
  }
}

/* Đã khảo sát tốc độ chạy thì có thể thấy là kể cả khi mảng ngẫu nhiên hay mảng đã được sắp xếp tốc độ chạy đều như nhau hết */
