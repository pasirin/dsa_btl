package Week5;

import java.util.Scanner;

public class QueueTwoStack {
  public static void main(String[] args) {
    Scanner temp = new Scanner(System.in);
    int N = temp.nextInt();
    Stack<Integer> Front = new Stack<>();
    Stack<Integer> End = new Stack<>();
    for (int i = 0; i < N; ++i) {
      int command = temp.nextInt();
      if (command == 1) {
        int data = temp.nextInt();
        End.push(data);
      } else {
        if (Front.isEmpty()) {
          while (!End.isEmpty()) {
            Front.push(End.pop());
          }
        }
        if (!Front.isEmpty()) {
          if (command == 2) {
            Front.pop();
          }
          if (command == 3) {
            System.out.println(Front.peek());
          }
        }
      }
    }
  }
}
