package Week5;

import java.util.Scanner;

public class EqualStacks {
  public static void main(String[] args) {
    Stack<Integer> stack1 = new Stack<>();
    Stack<Integer> stack2 = new Stack<>();
    Stack<Integer> stack3 = new Stack<>();
    Scanner scan = new Scanner(System.in);
    int N1 = scan.nextInt();
    int N2 = scan.nextInt();
    int N3 = scan.nextInt();
    int sum1 = 0, sum2 = 0, sum3 = 0;
    int temp;
    for (int i = 0; i < N1; i++) {
      temp = scan.nextInt();
      stack1.push(temp);
      sum1 += temp;
    }
    for (int i = 0; i < N2; i++) {
      temp = scan.nextInt();
      stack2.push(temp);
      sum2 += temp;
    }
    for (int i = 0; i < N3; i++) {
      temp = scan.nextInt();
      stack3.push(temp);
      sum3 += temp;
    }

    Stack<Integer> stack1rev = new Stack<>();
    Stack<Integer> stack2rev = new Stack<>();
    Stack<Integer> stack3rev = new Stack<>();
    while (!stack1.isEmpty()) {
      stack1rev.push(stack1.pop());
    }
    while (!stack2.isEmpty()) {
      stack2rev.push(stack2.pop());
    }
    while (!stack3.isEmpty()) {
      stack3rev.push(stack3.pop());
    }

    while (!(sum1 == sum2 && sum2 == sum3)) {
      int min = Math.min(sum1, Math.min(sum2, sum3));
      if (sum1 > min) {
        sum1 -= stack1rev.pop();
      }
      if (sum2 > min) {
        sum2 -= stack2rev.pop();
      }
      if (sum3 > min) {
        sum3 -= stack3rev.pop();
      }
    }
    System.out.println(sum1);
  }
}
