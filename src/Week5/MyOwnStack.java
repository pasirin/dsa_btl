package Week5;

import java.util.NoSuchElementException;

public class MyOwnStack {
  public static class LinkedListStack {
    private int numElement;
    private Node head;

    public LinkedListStack() {
      this.numElement = 0;
      this.head = null;
    }

    public boolean isEmpty() {
      return head == null;
    }

    public int size() {
      return this.numElement;
    }

    public void push(int data) {
      Node new_head = new Node(data);
      new_head.next = this.head;
      this.head = new_head;
      this.numElement++;
    }

    public int pop() {
      if (isEmpty()) throw new NoSuchElementException("The Stack Is Empty.");
      int return_data = head.data;
      head = head.next;
      numElement--;
      return return_data;
    }

    public int peek() {
      if (isEmpty()) throw new NoSuchElementException("The Stack Is Empty So No Peeking.");
      return head.data;
    }
  }

  public static class ArrayStack {
    private static final int default_capacity = 8;

    private Integer[] array;
    private int numElement;

    public ArrayStack() {
      array = new Integer[default_capacity];
      numElement = 0;
    }

    public boolean isEmpty() {
      return numElement == 0;
    }

    public int size() {
      return numElement;
    }

    private void resize(int newSize) {
      Integer[] new_array = new Integer[newSize];
      if (this.numElement >= 0) System.arraycopy(this.array, 0, new_array, 0, this.numElement);
      array = new_array;
    }

    public void push(int data) {
      if (this.numElement == array.length) {
        resize(2 * array.length);
      }
      array[numElement++] = data;
    }

    public int pop() {
      if (isEmpty()) {
        throw new NoSuchElementException("The Stack Is Empty.");
      }
      int return_data = array[numElement - 1];
      array[numElement - 1] = null;
      numElement--;
      if (numElement > 0 && numElement == array.length / 4) resize(array.length / 2);
      return return_data;
    }

    public int peek() {
      if (isEmpty()) throw new NoSuchElementException("The Stack Is Empty So No Peeking.");
      return array[numElement - 1];
    }
  }

  public static class Node {
    int data;
    Node next;

    public Node(int data) {
      this.data = data;
      this.next = null;
    }
  }
}
