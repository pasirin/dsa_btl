package Week5;

public class BalancedBrackets {
  public static String isBalanced(String s) {
    if (s == null || s.length() % 2 != 0) return "NO";
    Stack<Character> mem = new Stack<>();
    for (int i = 0; i < s.length(); ++i) {
      char temp = s.charAt(i);
      if (temp == '{' || temp == '[' || temp == '(') {
        mem.push(temp);
      } else if (temp == '}' || temp == ']' || temp == ')') {
        if (mem.isEmpty()) {
          return "NO";
        } else {
          char temp_new = mem.pop();
          if (temp_new == '{' && temp != '}') {
            return "NO";
          } else if (temp_new == '[' && temp != ']') {
            return "NO";
          } else if (temp_new == '(' && temp != ')') {
            return "NO";
          }
        }
      }
    }
    return mem.isEmpty()? "YES":"NO";
  }
}
