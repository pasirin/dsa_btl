package Week5;

import java.util.Scanner;

public class SimpleTextEditor {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    long N = scan.nextInt();
    Stack<String> mainStack = new Stack<>();
    for (long i = 0; i < N; i++) {
      int command = scan.nextInt();
      if (command == 1) {
        String preString = mainStack.size()>0 ? mainStack.peek(): "";
        String newString = preString + scan.next();
        mainStack.push(newString);
      } else if (command == 2) {
        int location = scan.nextInt();
        String preString = mainStack.peek();
        String newString = preString.substring(0, preString.length() - location);
        mainStack.push(newString);
      } else if (command == 3) {
        int location = scan.nextInt() - 1;
        if (mainStack.size()>0) {
          String preString = mainStack.peek();
          String charAtLoc = String.valueOf(preString.charAt(location));
          System.out.println(charAtLoc);
        }
      } else if (command == 4) {
        mainStack.pop();
      }
    }
  }
}
