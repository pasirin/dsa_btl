package Week3;

import edu.princeton.cs.algs4.*;

import java.util.Arrays;

public class ThreeSumFaster {
  public static void main(String[] args) {
    In in = new In(args[0]);
    int[] a = in.readAllInts();
    Arrays.sort(a, 0, a.length);
    for (int i = 0; i < a.length - 2; i++) {
      int x = a[i];
      int start = i + 1;
      int end = a.length - 1;
      while (start < end) {
        int b = a[start];
        int c = a[end];
        if (x + b + c == 0) {
          System.out.println(x + " " + b + " " + c);
          start = start + 1;
          end = end - 1;
        } else if (x + b + c > 0) {
          end = end - 1;
        } else {
          start = start + 1;
        }
      }
    }
  }
}

/*
  Độ phức tạp N^2
  chạy tệ nhất là khi không tìm được số nào
  !Không thể xử lý dãy có số lặp
 */
