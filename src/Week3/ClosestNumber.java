package Week3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ClosestNumber {
    public static List<Integer> closestNumbers(List<Integer> arr) {
        Collections.sort(arr);
        int dif=arr.get(1)-arr.get(0);
        for(int i=1;i<arr.size()-1;i++)
        {
            int temp = arr.get(i+1)-arr.get(i);
            if(dif>temp){
                dif=temp;
            }
        }
        List<Integer> result =new ArrayList<>();
        for(int i=0;i<arr.size()-1;i++)
        {
            int temp = arr.get(i+1)-arr.get(i);
            if(dif==temp){
                result.add(arr.get(i));
                result.add(arr.get(i+1));
            }
        }
        return result;
    }
}
/*
    Độ phức tạp N^2
    Tệ nhất và tốt nhất trùng nhau là N^2
 */