package Week3;

import java.util.List;

public class NewYearChaos {
  public static void minimumBribes(List<Integer> q) {
    int count = 0;
    for (int i = q.size() - 1; i >= 0; i--) {
      int result = q.get(i) - i - 1;
      if (result > 2) {
        System.out.println("Too chaotic");
        return;
      }
      for (int j = Math.max(0, q.get(i) - 2); j < i; j++) {
        if (q.get(j) > q.get(i)) count++;
      }
    }
    System.out.println(count);
  }
}
