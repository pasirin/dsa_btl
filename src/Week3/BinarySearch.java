package Week3;

import edu.princeton.cs.algs4.*;

import java.util.Arrays;

public class BinarySearch {
  public static int binarySearch(int[] a, int number) {
    int lowBound = 0, highBound = a.length - 1;
    while (lowBound <= highBound) {
      int index = lowBound + (highBound - lowBound) / 2;
      if (number < a[index]) highBound = index - 1;
      else if (number > a[index]) lowBound = index + 1;
      else return index;
    }
    return -1;
  }

  public static void main(String[] args) {
    In in = new In(args[0]);
    int[] a = in.readAllInts();
    Arrays.sort(a, 0, a.length);
    int lookFor = StdIn.readInt();
    int result = binarySearch(a, lookFor);
    if (result == -1) {
      StdOut.println("Could not find the number");
    } else {
      StdOut.println("The result is: " + a[result]);
    }
  }
}

/*
  Trường hợp tốt nhất chạy được với 1 lần duyệt
  Tệ nhất với việc không tồn tại kết quả.
  !Không thể xử lý số lặp
 */