package Week3;

import java.util.List;

public class SherlockAndArray {
    public static String balancedSums(List<Integer> arr) {
        int x=arr.size(),left=0,right=0;
        for (Integer integer : arr) {
            left += integer;
        }
        for(int i=0;i<x;i++)
        {
            left-=arr.get(i);
            if(i>0){right+=arr.get(i-1);}
            if(left==right){return "YES";}
        }
        return "NO";
    }
}

/*
    chạy tệ nhất khi cả 2 vế không bằng nhau được
    chạy tốt nhất khi số cuối mảng bằng tổng toàn bộ mảng trừ đi số cuối
    độ phức tạp tệ nhất là N^2
    tốt nhất là N+1
 */