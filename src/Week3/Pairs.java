package Week3;

import java.util.Collections;
import java.util.List;

public class Pairs {
  public static int pairs(int k, List<Integer> arr) {
    Collections.sort(arr);
    int count = 0;
    int i = 0, j = 1, n = arr.size();
    while (j < n) {
      int diff = arr.get(j) - arr.get(i);
      if (diff == k) {
        count++;
        j++;
      } else if (diff > k) {
        i++;
      } else {
        j++;
      }
    }
    return count;
  }
}

/*
    Tốt nhất là N lần
    Tệ nhất là N^2 lần chạy
 */