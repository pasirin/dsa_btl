package Week6;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdArrayIO;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class MergeSort {
  private static void merge(int[] a, int[] aux, int lo, int mid, int hi) {
    for (int k = lo; k <= hi; k++) {
      aux[k] = a[k];
    }
    int i = lo, j = mid + 1;
    for (int k = lo; k <= hi; k++) {
      if (i > mid) {
        a[k] = aux[j++];
      } else if (j > hi) {
        a[k] = aux[i++];
      } else if (aux[j] < aux[i]) {
        a[k] = aux[j++];
      } else {
        a[k] = aux[i++];
      }
    }
  }

  private static void sort(int[] a, int[] aux, int lo, int hi) {
    if (hi <= lo) return;
    int mid = lo + (hi - lo) / 2;
    sort(a, aux, lo, mid);
    sort(a, aux, mid + 1, hi);
    merge(a, aux, lo, mid, hi);
  }

  public static void sort(int[] a) {
    int[] aux = new int[a.length];
    sort(a, aux, 0, a.length - 1);
  }

  public static void main(String[] args) {
    In in = new In("lib/algs4/32Kints.txt");
    int[] a = in.readAllInts();

    long start = System.currentTimeMillis();
    sort(a);
    long end = System.currentTimeMillis();

    System.out.println(end - start);
    StdArrayIO.print(a);
  }
}
/* Thời Gian Chạy điều kiện (1) trung bình là: 1k-0 2k-0 4k-1 8k-2 16k-4 32k-8 */
/* Thời Gian Chạy điều kiện (2) trung bình là: 1k-1 2k-1 4k-2 8k-2 16k-3 32k-7 */
/* Thời Gian Chạy điều kiện (3) trung bình là: 1k-0 2k-0 4k-1 8k-2 16k-2 32k-5 */
/* Thời Gian Chạy điều kiện (4) trung bình là: 1k-0 2k-0 4k-1 8k-1 16k-3 32k-5 */
/* Thời Gian Chạy điều kiện (5) trung bình là: 1k-1 2k-1 4k-1 8k-2 16k-2 32k-5 */
