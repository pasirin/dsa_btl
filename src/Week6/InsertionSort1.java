package Week6;

import java.util.List;

public class InsertionSort1 {
  public static void insertionSort1(int n, List<Integer> arr) {
    int move = arr.get(n - 1);
    int i = n - 1;
    boolean check = false;
    while (!check) {
      if (i == 0) {
        arr.set(0, move);
        check = true;
      } else if (arr.get(i - 1) > move) {
        arr.set(i, arr.get(i - 1));
        i--;
      } else {
        arr.set(i, move);
        check = true;
      }
      for (Integer integer : arr) {
        System.out.print(integer + " ");
      }
      System.out.print("\n");
    }
  }
}
