package Week6;

import edu.princeton.cs.algs4.*;

import java.util.Arrays;
import java.util.Collections;

public class InsertionSort {
  public static void main(String[] args) {
    In in = new In("lib/algs4/1Kints.txt");
    int[] a = in.readAllInts();
    int n = a.length;

    long start = System.currentTimeMillis();
    for (int i = 0; i < n; i++) {
      for (int j = i; j > 0; j--) {
        if (a[j] < a[j - 1]) {
          int temp = a[j];
          a[j] = a[j - 1];
          a[j - 1] = temp;
        } else {
          break;
        }
      }
    }
    long end = System.currentTimeMillis();

    System.out.println(end - start);
    StdArrayIO.print(a);
  }
}
/* Thời Gian Chạy điều kiện (1) trung bình là: 1k-2 2k-6 4k-10 8k-21 16k-38 32k-142 */
/* Thời Gian Chạy điều kiện (2) trung bình là: 1k-2 2k-7 4k-10 8k-14 16k-45 32k-189 */
/* Thời Gian Chạy điều kiện (3) trung bình là: 1k-0 2k-0 4k-0 8k-0 16k-1 32k-1 */
/* Thời Gian Chạy điều kiện (4) trung bình là: 1k-14 2k-31 4k-68 8k-210 16k-853 32k-8234 */
/* Thời Gian Chạy điều kiện (5) trung bình là: 1k-0 2k-0 4k-0 8k-0 16k-0 32k-1 */
