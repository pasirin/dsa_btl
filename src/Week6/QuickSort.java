package Week6;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdArrayIO;
import edu.princeton.cs.algs4.StdRandom;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class QuickSort {

  private static int patition(int[] a, int lo, int hi) {
    int i = lo, j = hi + 1;
    while (true) {
      while (a[++i] < a[lo]) {
        if (i == hi) break;
      }

      while (a[lo] < a[--j]) {
        if (j == lo) break;
      }

      if (i >= j) break;
      exch(a, i, j);
    }

    exch(a, lo, j);
    return j;
  }

  public static void sort(int[] a) {
    // StdRandom.shuffle(a);
    sort(a, 0, a.length - 1);
  }

  private static void sort(int[] a, int lo, int hi) {
    if (hi <= lo) return;
    int j = patition(a, lo, hi);
    sort(a, lo, j - 1);
    sort(a, j + 1, hi);
  }

  private static void exch(int[] a, int i, int j) {
    int swap = a[i];
    a[i] = a[j];
    a[j] = swap;
  }

  public static void main(String[] args) {
    In in = new In("lib/algs4/32Kints.txt");
    int[] a = in.readAllInts();

    long start = System.currentTimeMillis();
    sort(a);
    long end = System.currentTimeMillis();

    System.out.println(end - start);
    StdArrayIO.print(a);
  }
}
/* Thời Gian Chạy có Shuffle */
/* Thời Gian Chạy điều kiện (1) trung bình là: 1k-2 2k-3 4k-4 8k-5 16k-5 32k-8 */
/* Thời Gian Chạy điều kiện (2) trung bình là: 1k-1 2k-2 4k-2 8k-4 16k-4 32k-10 */
/* Thời Gian Chạy điều kiện (3) trung bình là: 1k-2 2k-2 4k-4 8k-5 16k-10 32k-12 */
/* Thời Gian Chạy điều kiện (4) trung bình là: 1k-1 2k-3 4k-3 8k-4 16k-4 32k-13 */
/* Thời Gian Chạy điều kiện (5) trung bình là: 1k-2 2k-4 4k-4 8k-5 16k-5 32k-11 */
/* Thời Gian Chạy Khum có Shuffle */
/* Thời Gian Chạy điều kiện (1) trung bình là: 1k-1 2k-1 4k-2 8k-2 16k-3 32k-6 */
/* Thời Gian Chạy điều kiện (2) trung bình là: 1k-0 2k-1 4k-2 8k-2 16k-2 32k-5 */
/* Thời Gian Chạy điều kiện (3) trung bình là: 1k-3 2k-7 4k-15 8k-26 16k-115 32k-lỗi StackOverFlow */
/* Thời Gian Chạy điều kiện (4) trung bình là: 1k-3 2k-12 4k-13 8k-22 16k-84 32k-lỗi StackOverFlow */
/* Thời Gian Chạy điều kiện (5) trung bình là: 1k-0 2k-0 4k-1 8k-1 16k-2 32k-3 */
