package Week6;

import java.util.ArrayList;
import java.util.List;

public class InsertionSort2 {
  public static void insertionSort2(int n, List<Integer> arr) {
    for (int k = 0; k < n; k++) {
      System.out.print(arr.get(k) + " ");
    }
    System.out.println("");
    for (int i = 0; i < n; i++) {
      for (int j = i; j > 0; j--) {
        if (arr.get(j) < arr.get(j - 1)) {
          int temp = arr.get(j);
          arr.set(j, arr.get(j - 1));
          arr.set(j - 1, temp);
          for (int k = 0; k < n; k++) {
            System.out.print(arr.get(k) + " ");
          }
          System.out.println("");
        } else {
          break;
        }
      }
    }
  }
}
